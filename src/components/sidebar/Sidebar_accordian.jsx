import "./sidebar.css";
import DashboardIcon from "@mui/icons-material/Dashboard";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import LocalShippingIcon from "@mui/icons-material/LocalShipping";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import StoreIcon from "@mui/icons-material/Store";
import InsertChartIcon from "@mui/icons-material/InsertChart";
import SettingsApplicationsIcon from "@mui/icons-material/SettingsApplications";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import SettingsSystemDaydreamOutlinedIcon from "@mui/icons-material/SettingsSystemDaydreamOutlined";
import PsychologyOutlinedIcon from "@mui/icons-material/PsychologyOutlined";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import { Link } from "react-router-dom";
import { DarkModeContext } from "../../context/darkModeContext";
import { useContext, useState } from "react";

const SidebarAcc = () => {
  const { dispatch } = useContext(DarkModeContext);
  const [isRootAccountActive, setIsRootAccountActive] = useState(false);
  const [isClusterutilizationActive, setIsClusterutilizationActive] = useState(false);
  const [issystemgroupsActive, setIssystemgroupsActive] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const [isCluster, setIsCluster] = useState(false);
  const [isImageGropus, setIsImageGropussetIsImageGropus] = useState(false);
  const [iscentosPbs, setIscentosPbs] = useState(false);
  const [isCentosIdp, setIsCentosIdpsetIsCentosIdp] = useState(false);
  const [isManagementCards, setIsManagementCards] = useState(false);
  // const [isRootAccountActive, setIsRootAccountActive] = useState(false);
  // const [isClusterutilizationActive, setIsClusterutilizationActive] = useState(false);
  // const [issystemgroupsActive, setIssystemgroupsActive] = useState(false);
  // const [isAdmin, setIsAdmin] = useState(false);
  // const [isActive, setIsActive] = useState(false);
  // const [isCluster, setIsCluster] = useState(false);
  // const [isActive, setIsActive] = useState(false);
  // const [isActive, setIsActive] = useState(false);
  // const [isActive, setIsActive] = useState(false);
  // const [isActive, setIsActive] = useState(false);

//   
// 	Clusterutilization
// System Groups
// 	Admin
// 	Cluster
// Network Groups 
// Image Groups 
// 	Centos pbs
// 	Centos ldap
// Custom groups
// Archived Groups
// Nodes defination
// Nodes
// Networks
// Management Cards
// 	iLO
// 	io100i
// 	ILOCM
// 	IPMI
// Metrics Scripts

  // function toggleAccordion() {
  //   setIsOpen(!isOpen);
  // }


  return (
    <div className="sidebar">
      <div className="top">
        <Link to="/" style={{ textDecoration: "none" }}>
          <span className="logo">lamadmin</span>
        </Link>
      </div>
      <hr />
      <div className="center">
        <ul>
          <p className="title">MAIN</p>
          <li>

{/* Accordian code  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIsRootAccountActive(!isRootAccountActive)}>
            <div>
            <DashboardIcon className="icon" />
            <span>Root account</span>
            </div>
            <div>{isRootAccountActive ? '-' : '+'}</div>
    </div>
    {isRootAccountActive && <div className="accordion-content">
      <ul>
      <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIsClusterutilizationActive(!isClusterutilizationActive)}>
            <div>
            <PersonOutlineIcon className="icon" />
              <span>Cluster utilization</span>
            </div>
            <div>{isClusterutilizationActive ? '-' : '+'}</div>
    </div>
    {isClusterutilizationActive && <div className="accordion-content">Lorem ipsum dolor sit amet consectetur, </div>}
        </div>
      </div>
{/* CODE END  */}

            </li>

      </ul>
      </div>}
        </div>
      </div>
{/* code end  */}

          </li>
          <p className="title">LISTS</p>


          
          {/* <Link to="/users" style={{ textDecoration: "none" }}> */}
            <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIssystemgroupsActive(!issystemgroupsActive)}>
            <div>
            <PersonOutlineIcon className="icon" />
              <span>System Groups</span>
            </div>
            <div>{issystemgroupsActive ? '-' : '+'}</div>
    </div>
    {issystemgroupsActive && <div className="accordion-content">
      
    <ul>
      <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIsAdmin(!isAdmin)}>
            <div>
            <PersonOutlineIcon className="icon" />
              <span>Admin</span>
            </div>
            <div>{isAdmin ? '-' : '+'}</div>
    </div>
    {isAdmin && <div className="accordion-content">Lorem ipsum dolor sit amet consectetur, </div>}
        </div>
      </div>
{/* CODE END  */}

            </li>

            <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIsCluster(!isCluster)}>
            <div>
            <PersonOutlineIcon className="icon" />
              <span>Cluster</span>
            </div>
            <div>{isCluster ? '-' : '+'}</div>
    </div>
    {isCluster && <div className="accordion-content">Lorem ipsum dolor sit amet consectetur, </div>}
        </div>
      </div>
{/* CODE END  */}

            </li>

      </ul>
      </div>}
        </div>
      </div>
{/* CODE END  */}

            </li>

            <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIsActive(!isActive)}>
            <div>
            <StoreIcon className="icon" />
              <span>Network groups</span>
            </div>
            <div>{isActive ? '-' : '+'}</div>
    </div>
    {isActive && <div className="accordion-content"></div>}
        </div>
      </div>
{/* CODE END  */}

            </li>

            <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIsImageGropussetIsImageGropus(!isImageGropus)}>
            <div>
            <CreditCardIcon className="icon" />
            <span>Image groups</span>
            </div>
            <div>{isImageGropus ? '-' : '+'}</div>
    </div>
    {isImageGropus && <div className="accordion-content">
      
    <ul>
      <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIscentosPbs(!iscentosPbs)}>
            <div>
            <PersonOutlineIcon className="icon" />
              <span>Centos pbs</span>
            </div>
            <div>{iscentosPbs ? '-' : '+'}</div>
    </div>
    {iscentosPbs && <div className="accordion-content">Lorem ipsum dolor sit amet consectetur, </div>}
        </div>
      </div>
{/* CODE END  */}

            </li>

            <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIsCentosIdpsetIsCentosIdp(!isCentosIdp)}>
            <div>
            <PersonOutlineIcon className="icon" />
              <span>Centos Idap</span>
            </div>
            <div>{isCentosIdp ? '-' : '+'}</div>
    </div>
    {isCentosIdp && <div className="accordion-content">Lorem ipsum dolor sit amet consectetur, </div>}
        </div>
      </div>
{/* CODE END  */}

            </li>

      </ul>

      </div>}
        </div>
      </div>
{/* CODE END  */}

            </li>

            <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIsActive(!isActive)}>
            <div>
            <LocalShippingIcon className="icon" />
            <span>Custom gropus</span>
            </div>
            <div>{isActive ? '-' : '+'}</div>
    </div>
    {isActive && <div className="accordion-content"></div>}
        </div>
      </div>
{/* CODE END  */}

            </li>


            <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIsActive(!isActive)}>
            <div>
            <InsertChartIcon className="icon" />
            <span>Archived groups</span>
            </div>
            <div>{isActive ? '-' : '+'}</div>
    </div>
    {isActive && <div className="accordion-content"></div>}
        </div>
      </div>
{/* CODE END  */}

            </li>
          {/* </Link> */}



          {/* <Link to="/products" style={{ textDecoration: "none" }}>
            <li>
              <StoreIcon className="icon" />
              <span>Products</span>
            </li>
          </Link>
          <li>
            <CreditCardIcon className="icon" />
            <span>Orders</span>
          </li>
          <li>
            <LocalShippingIcon className="icon" />
            <span>Delivery</span>
          </li> */}


          <p className="title">USEFUL</p>

          {/* <li>
            <InsertChartIcon className="icon" />
            <span>Stats</span>
          </li>
          <li>
            <NotificationsNoneIcon className="icon" />
            <span>Notifications</span>
          </li> */}


            <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIsActive(!isActive)}>
            <div>
            <NotificationsNoneIcon className="icon" />
            <span>Nodes defination</span>
            </div>
            <div>{isActive ? '-' : '+'}</div>
    </div>
    {isActive && <div className="accordion-content"></div>}
        </div>
      </div>
{/* CODE END  */}

            </li>


          <p className="title">SERVICE</p>


          {/* <li>
            <SettingsSystemDaydreamOutlinedIcon className="icon" />
            <span>System Health</span>
          </li>
          <li>
            <PsychologyOutlinedIcon className="icon" />
            <span>Logs</span>
          </li>
          <li>
            <SettingsApplicationsIcon className="icon" />
            <span>Settings</span>
          </li> */}


<li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIsActive(!isActive)}>
            <div>
            <SettingsSystemDaydreamOutlinedIcon className="icon" />
            <span>Nodes</span>
            </div>
            <div>{isActive ? '-' : '+'}</div>
    </div>
    {isActive && <div className="accordion-content"></div>}
        </div>
      </div>
{/* CODE END  */}

            </li>


            <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item" onClick={() => setIsActive(!isActive)}>
          <div className="custom-accordion-title" >
            <div>
            <PsychologyOutlinedIcon className="icon" />
            <span>Networks</span>
            </div>
            <div>{isActive ? '-' : '+'}</div>
    </div>
    {isActive && <div className="accordion-content"></div>}
        </div>
      </div>
{/* CODE END  */}

            </li>

            <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIsManagementCards(!isManagementCards)}>
            <div>
            <SettingsApplicationsIcon className="icon" />
            <span>Management cards</span>
            </div>
            <div>{isManagementCards ? '-' : '+'}</div>
    </div>
    {isManagementCards && <div className="accordion-content">
      
    <ul>
      <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" >
            <div>
            <PersonOutlineIcon className="icon" />
              <span>iLO</span>
            </div>
            {/* <div>{iscentosPbs ? '-' : '+'}</div> */}
    </div>
    {/* {iscentosPbs && <div className="accordion-content">Lorem ipsum dolor sit amet consectetur, </div>} */}
        </div>
      </div>
{/* CODE END  */}

            </li>

            <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" >
            <div>
            <PersonOutlineIcon className="icon" />
              <span>io100i</span>
            </div>
            {/* <div>{isCentosIdp ? '-' : '+'}</div> */}
    </div>
    {/* {isCentosIdp && <div className="accordion-content">Lorem ipsum dolor sit amet consectetur, </div>} */}
        </div>
      </div>
{/* CODE END  */}

            </li>

            <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" >
            <div>
            <PersonOutlineIcon className="icon" />
              <span>ILOCM</span>
            </div>
            {/* <div>{iscentosPbs ? '-' : '+'}</div> */}
    </div>
    {/* {iscentosPbs && <div className="accordion-content">Lorem ipsum dolor sit amet consectetur, </div>} */}
        </div>
      </div>
{/* CODE END  */}

            </li>

            <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" >
            <div>
            <PersonOutlineIcon className="icon" />
              <span>IPMI</span>
            </div>
            {/* <div>{isCentosIdp ? '-' : '+'}</div> */}
    </div>
    {/* {isCentosIdp && <div className="accordion-content">Lorem ipsum dolor sit amet consectetur, </div>} */}
        </div>
      </div>
{/* CODE END  */}

            </li>

      </ul>
      
      </div>}
        </div>
      </div>
{/* CODE END  */}

            </li>

            <li>
{/* ACCORDIAN CODE  */}
<div className="custom-accordion w-100" >
        <div className="custom-accordion-item">
          <div className="custom-accordion-title" onClick={() => setIsActive(!isActive)}>
            <div>
            <SettingsApplicationsIcon className="icon" />
            <span>Metrics scripts</span>
            </div>
            <div>{isActive ? '-' : '+'}</div>
    </div>
    {isActive && <div className="accordion-content"></div>}
        </div>
      </div>
{/* CODE END  */}

            </li>

          


          <p className="title">USER</p>
          <li>
            <AccountCircleOutlinedIcon className="icon" />
            <span>Profile</span>
          </li>
          <li>
            <ExitToAppIcon className="icon" />
            <span>Logout</span>
          </li>
        </ul>
      </div>
      <div className="bottom">
        <div
          className="colorOption"
          onClick={() => dispatch({ type: "LIGHT" })}
        ></div>
        <div
          className="colorOption"
          onClick={() => dispatch({ type: "DARK" })}
        ></div>
      </div>
    </div>
  );
};

export default SidebarAcc;
