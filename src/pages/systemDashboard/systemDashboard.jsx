import React from 'react'
import "./system.css"
import SidebarAcc from '../../components/sidebar/Sidebar_accordian'
import Navbar from '../../components/navbar/Navbar'
import areaChart from '../../components/areaChart/AreaChart1'
import SDAreaCharts from '../../components/SystemDashboardCharts/SDAreaCharts'
import Chart from '../../components/chart/Chart'

export default function SystemDashboard() {
  return (


    <div className="home">
    {/* <Sidebar /> */}
    <SidebarAcc></SidebarAcc>
    <div className="homeContainer">
      {/* <Navbar /> */}
      <Navbar></Navbar>
      <div className='systemDashboard'>
        {/* <div><areaChart></areaChart></div>
        <div><areaChart></areaChart></div> */}

        {/* Im just typing somethings */}
        <Chart title="Last 6 Months (Revenue)" aspect={2 / 1} />

    </div>
    </div>
  </div>



  )
}
